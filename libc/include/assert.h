#ifndef _ASSERT_H
#define _ASSERT_H 1

#ifdef NDEBUG
#define assert(condition) ((void)0)
#else
#define assert(condition) /*implementation defined*/
#endif

#endif
