#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/orion.kernel isodir/boot/orion.kernel
cp sysroot/boot/initrd.img isodir/boot/initrd.img
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "orion" {
	multiboot /boot/orion.kernel
	module  /boot/initrd.img
}
EOF
grub-mkrescue -o orion.iso isodir
